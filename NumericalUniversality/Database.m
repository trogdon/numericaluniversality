(* ::Package:: *)

(************************************************************************)
(* This file was generated automatically by the Mathematica front end.  *)
(* It contains Initialization cells from a Notebook file, which         *)
(* typically will have the same name as this file except ending in      *)
(* ".nb" instead of ".m".                                               *)
(*                                                                      *)
(* This file is intended to be loaded into the Mathematica kernel using *)
(* the package loading commands Get or Needs.  Doing so is equivalent   *)
(* to using the Evaluate Initialization Cells menu command in the front *)
(* end.                                                                 *)
(*                                                                      *)
(* DO NOT EDIT THIS FILE.  This entire file is regenerated              *)
(* automatically each time the parent Notebook file is saved in the     *)
(* Mathematica front end.  Any changes you make to this file will be    *)
(* overwritten.                                                         *)
(************************************************************************)



GetDist[stringMethod_,stringEnsemble_,n_,tol_,m_]:=EmpiricalDistribution[HaltingDatabase[stringMethod,stringEnsemble,n,tol,m]];
GetNormalizedDist[stringMethod_,stringEnsemble_,n_,tol_,m_]:=EmpiricalDistribution[HaltingDatabase[stringMethod,stringEnsemble,n,tol,m]//((#-Mean[#])/StandardDeviation[#])&];
ParallelTrue[]:=Module[{},parallel=True];
ParallelFalse[]:=Module[{},parallel=False];
ParallelTrue[];


(* Works for all methods besides Genetic and Curie-Weiss because different filenames are used for those cases *)
HaltingDatabase[stringMethod_,stringEnsemble_,n_,tol_,All]:=
Module[{},
If[FindFile[
FileNameJoin[{workingdir,"HaltingDatabase",stringMethod,stringEnsemble,ToString[n]<>"-"<>ToString[tol]<>".m"}]]===$Failed,
{},
Get[FileNameJoin[{workingdir,"HaltingDatabase",stringMethod,stringEnsemble,ToString[n]<>"-"<>ToString[tol]<>".m"}]]]
];
HaltingDatabase[stringMethod_,stringEnsemble_,n_,tol_,m_]:=Module[{data,ii,add,counter,mm},
data=HaltingDatabase[stringMethod,stringEnsemble,n,tol,All];
mm=m;
If[stringEnsemble=="CoshUE",SetCoshUECounter[n,tol];ParallelFalse[];mm=Min[m,CoshLen[n]];];
If[stringEnsemble=="QUE",SetQUECounter[n,tol];ParallelFalse[];mm=Min[m,QuarticLen[n]];];

If[m>Length[data],
Print["Getting "<>ToString[m-Length[data]]<>" more samples."];
counter=0;
localcounter=0;
If[parallel,
SetSharedVariable[counter];
SampleHaltingTime[stringMethod,stringEnsemble,n,tol];
ParallelEvaluate[localcounter=0;];
add=ParallelTable[
localcounter++;
If[Mod[ii,1000]==0,counter+=localcounter;localcounter=0;
Print["Have " <> ToString[Length[data]+counter]<> " samples."];
];
SampleHaltingTime[stringMethod,stringEnsemble,n,tol],{ii,m-Length[data]+10}];,
add=
Table[
If[Mod[ii,1000]==0,localcounter+=1000;
Print["Have " <> ToString[Length[data]+localcounter]<> " samples."];
];
SampleHaltingTime[stringMethod,stringEnsemble,n,tol],{ii,m-Length[data]+10}];

];
data=DeleteCases[data~Join~add,a_/;a ==Null];
Put[data,FileNameJoin[{workingdir,"HaltingDatabase",stringMethod,stringEnsemble,ToString[n]<>"-"<>ToString[tol]<>".m"}]];
];
data[[;;mm]]
]


SampleHaltingTime["Jacobi",string_,n_,tol_]:=Module[{GetEnsemble,out,A},
If[string=="GOE",GetEnsemble=GOE];
If[string=="BE",GetEnsemble=BE];
If[string=="test",GetEnsemble=test];
A=GetEnsemble[n];
out=JacobiIterationWithDeflateOffDiagonal[A,\[Infinity],Sqrt[n]10^(-tol)];
out[[-1]][[-1]][[1]]
]


SampleHaltingTime["QR",string_,n_,tol_]:=Module[{GetEnsemble,out,A},
If[string=="GOE",GetEnsemble=GOE];
If[string=="GUE",GetEnsemble=GUE];
If[string=="QUE",GetEnsemble=QUE];
If[string=="sGUE",GetEnsemble=sGUE];
If[string=="sQUE",GetEnsemble=sQUE];
If[string=="CoshUE", GetEnsemble=CoshUE];
If[string=="test",GetEnsemble=test];
If[string=="b12", GetEnsemble=b12];
If[string=="b13",GetEnsemble=b13];
If[string=="iid",GetEnsemble=iid];
If[string=="BE",GetEnsemble=BE];
A=GetEnsemble[n];
If[A==0,Print["# of requested samples exceeded # in SpectraDatabase, counter undefined or no SpectraDatabase entry exists"];
Return[Null];];
out=QR[A,10^(-tol)]
];


SampleHaltingTime["Power",string_,n_,tol_]:=Module[{GetEnsemble,out,A},
If[string=="GOE",GetEnsemble=GOE];
If[string=="GUE",GetEnsemble=GUE];
If[string=="QUE",GetEnsemble=QUE];
If[string=="sGUE",GetEnsemble=sGUE];
If[string=="sQUE",GetEnsemble=sQUE];
If[string=="CoshUE", GetEnsemble=CoshUE];
If[string=="test",GetEnsemble=test];
If[string=="b12", GetEnsemble=b12];
If[string=="b13",GetEnsemble=b13];
If[string=="iid",GetEnsemble=iid];
A=GetEnsemble[n];
If[A==0,Print["# of requested samples exceeded # in SpectraDatabase, counter undefined or no SpectraDatabase entry exists"];
Return[Null];];
out=PowerMethodActual[A,10^(-tol)]
];


SampleHaltingTime["ODE",string_,n_,tol_]:=Module[{GetEnsemble,out,A},
If[string=="GOE",GetEnsemble=GOE];
If[string=="GUE",GetEnsemble=GUE];
If[string=="QUE",GetEnsemble=QUE];
If[string=="sGUE",GetEnsemble=sGUE];
If[string=="sQUE",GetEnsemble=sQUE];
If[string=="CoshUE", GetEnsemble=CoshUE];
If[string=="test",GetEnsemble=test];
If[string=="b12", GetEnsemble=b12];
If[string=="b13",GetEnsemble=b13];
If[string=="iid",GetEnsemble=iid];
A=GetEnsemble[n];
If[A==0,Print["# of requested samples exceeded # in SpectraDatabase, counter undefined or no SpectraDatabase entry exists"];
Return[Null];];
out=ODEMethodActual[A,10^(-tol)]
];


SampleHaltingTime["ODEGuess",string_,n_,tol_]:=Module[{GetEnsemble,out,A},
If[string=="GOE",GetEnsemble=GOE];
If[string=="GUE",GetEnsemble=GUE];
If[string=="QUE",GetEnsemble=QUE];
If[string=="sGUE",GetEnsemble=sGUE];
If[string=="sQUE",GetEnsemble=sQUE];
If[string=="CoshUE", GetEnsemble=CoshUE];
If[string=="test",GetEnsemble=test];
If[string=="b12", GetEnsemble=b12];
If[string=="b13",GetEnsemble=b13];
If[string=="iid",GetEnsemble=iid];
A=GetEnsemble[n];
If[A==0,Print["# of requested samples exceeded # in SpectraDatabase, counter undefined or no SpectraDatabase entry exists"];
Return[Null];];
out=ODEMethodGuess[A,10^(-tol)]
];


SampleHaltingTime["InvPower",string_,n_,tol_]:=Module[{GetEnsemble,out,A},
If[string=="GOE",GetEnsemble=GOE];
If[string=="GUE",GetEnsemble=GUE];
If[string=="QUE",GetEnsemble=QUE];
If[string=="sGUE",GetEnsemble=sGUE];
If[string=="sQUE",GetEnsemble=sQUE];
If[string=="CoshUE", GetEnsemble=CoshUE];
If[string=="test",GetEnsemble=test];
If[string=="b12", GetEnsemble=b12];
If[string=="b13",GetEnsemble=b13];
If[string=="iid",GetEnsemble=iid];
A=GetEnsemble[n];
If[A==0,Print["# of requested samples exceeded # in SpectraDatabase, counter undefined or no SpectraDatabase entry exists"];
Return[Null];];
out=InvPowerMethodActual[A,10^(-tol)]
];


SampleHaltingTime["PowerError",string_,n_,tol_]:=Module[{GetEnsemble,out,A},
If[string=="GOE",GetEnsemble=GOE];
If[string=="GUE",GetEnsemble=GUE];
If[string=="QUE",GetEnsemble=QUE];
If[string=="sGUE",GetEnsemble=sGUE];
If[string=="sQUE",GetEnsemble=sQUE];
If[string=="CoshUE", GetEnsemble=CoshUE];
If[string=="test",GetEnsemble=test];If[string=="b12", GetEnsemble=b12];
If[string=="b13",GetEnsemble=b13];
If[string=="iid",GetEnsemble=iid];
A=GetEnsemble[n];
If[A==0,Print["# of requested samples exceeded # in SpectraDatabase, counter undefined or no SpectraDatabase entry exists"];
Return[Null];];
out=PowerMethodError[A,10^(-tol)]
];


sp[x_]:=SetPrecision[x,50];
SampleHaltingTime["CGHi",string_,n_,tol_]:=Module[{GetEnsemble,out,A},
If[string=="cLOE",GetEnsemble=cLOE];
If[string=="cPBE",GetEnsemble=cPBE];
If[string=="test",GetEnsemble=test];
If[string=="cLUE",GetEnsemble=cLUE];
If[string=="sLUE",GetEnsemble=sLUE];
If[string=="nLUE",GetEnsemble=nLUE];
If[string=="nPBE",GetEnsemble=nPBE];
If[string=="cnLUE",GetEnsemble=cnLUE];
A=GetEnsemble[n];
out=ConjugateGradientHi[A//sp,RandomReal[{-1,1},n]//sp,10^(-tol)]
];


SampleHaltingTime["CG",string_,n_,tol_]:=Module[{GetEnsemble,out,A},
If[string=="cLOE",GetEnsemble=cLOE];
If[string=="cPBE",GetEnsemble=cPBE];
If[string=="test",GetEnsemble=test];
If[string=="cLUE",GetEnsemble=cLUE];
If[string=="sLUE",GetEnsemble=sLUE];
If[string=="nLUE",GetEnsemble=nLUE];
If[string=="cnLUE",GetEnsemble=cnLUE];
If[string=="nPBE",GetEnsemble=nPBE];
A=GetEnsemble[n];
out=ConjugateGradient[A,RandomReal[{-1,1},n],10^(-tol)]
];


SampleHaltingTime["CGA",string_,n_,tol_]:=Module[{GetEnsemble,out,A},
If[string=="cLOE",GetEnsemble=cLOE];
If[string=="cPBE",GetEnsemble=cPBE];
If[string=="test",GetEnsemble=test];
A=GetEnsemble[n];
out=ConjugateGradientA[A,RandomReal[{-1,1},n],10^(-tol)]
];


SampleHaltingTime["CGError",string_,n_,tol_]:=Module[{GetEnsemble,out,A},
If[string=="cLOE",GetEnsemble=cLOE];
If[string=="cPBE",GetEnsemble=cPBE];
If[string=="test",GetEnsemble=test];
A=GetEnsemble[n];
out=CGErrorBound[A,RandomReal[{-1,1},n],10^(-tol)]
];


SampleHaltingTime["CGAError",string_,n_,tol_]:=Module[{GetEnsemble,out,A},
If[string=="cLOE",GetEnsemble=cLOE];
If[string=="cPBE",GetEnsemble=cPBE];
If[string=="test",GetEnsemble=test];
A=GetEnsemble[n];
out=CGAErrorBound[A,RandomReal[{-1,1},n],10^(-tol)]
];


SampleHaltingTime["GMRES",string_,n_,tol_]:=Module[{GetEnsemble,out,A,f,inner},
If[string=="cSGE",GetEnsemble=cSGE];
If[string=="cSBE",GetEnsemble=cSBE];
If[string=="UDE",GetEnsemble=UDE];
If[string=="BDE",GetEnsemble=BDE];
If[string=="fUDE",GetEnsemble=fUDE];
If[string=="test",GetEnsemble=test];
A=GetEnsemble[n];
f[x_]:=A.x;
inner[x_,y_]:=Dot[x,Conjugate[y]];
out=GMRES[n,f,RandomReal[{-1,1},n],inner,10^(-tol)]
];


ReadExact[genes_,string_]:=Module[{a,i,plt,min,j},
If[FindFile[FileNameJoin[{workingdir,"HaltingDatabase","Genetic",string,"Exact",ToString[genes]<>".m"}]]===$Failed,Print["Do not have actual solution"];
Return[{}];,Get[FileNameJoin[{workingdir,"HaltingDatabase","Genetic",string,"Exact",ToString[genes]<>".m"}]]]
];
HaltingDatabase[{"Genetic",stringMethod_},stringEnsemble_,n_,tol_,All]:=
Module[{},
If[FindFile[
FileNameJoin[{workingdir,"HaltingDatabase","Genetic",stringMethod,stringEnsemble,ToString[n]<>"-"<>ToString[tol]<>".m"}]
]===$Failed,
{},
Get[
FileNameJoin[{workingdir,"HaltingDatabase","Genetic",stringMethod,stringEnsemble,ToString[n]<>"-"<>ToString[tol]<>".m"}]
]
]
];
HaltingDatabase[{"Genetic",stringMethod_},stringEnsemble_,n_,tol_,m_]:=Module[{data,ii,add,counter},
data=HaltingDatabase[{"Genetic",stringMethod},stringEnsemble,n,tol,All];
If[m>Length[data],
Print["Getting "<>ToString[m-Length[data]]<>" more samples."];
counter=0;
localcounter=0;
SetSharedVariable[counter];
ParallelEvaluate[localcounter=0;];
(*add ParallelTable here*)
add=Monitor[ParallelTable[
localcounter++;
If[Mod[ii,100]==0,counter+=localcounter;localcounter=0;
Print["Have " <> ToString[Length[data]+counter]<> " samples."];
];
SampleHaltingTime[stringMethod,stringEnsemble,n,tol],{ii,m-Length[data]+10}],Length[data]+counter];
data=data~Join~add;
Put[data,FileNameJoin[{workingdir,"HaltingDatabase","Genetic",stringMethod,stringEnsemble,ToString[n]<>"-"<>ToString[tol]<>".m"}]];
];
data[[;;m]]
];
SampleHaltingTime[{"Genetic",Potential_},string_,genes_,tol_]:=Module[{i,j,min,out,A,population,ChooseIndividual,ChooseMutation,ChooseGene,Distributions,exact},
If[Potential=="Gaussian",V[x_]:=x^2;,
If[Potential=="Split",V[x_]:=x^4-3x^2;,
Return[Null];
];
];

population=100;
ChooseIndividual[nn_]:=RandomInteger[{1,nn}];
ChooseGene[nn_]:=RandomInteger[{1,nn}];
If[string=="Uniform",ChooseMutation[nn_]:=RandomReal[{-1,1},nn] 0.1/genes;];
If[string=="Bernoulli",ChooseMutation[nn_]:=(2RandomInteger[{0,1},nn] -1)0.1/genes;];
Distributions={ChooseIndividual,ChooseGene,ChooseMutation};
exact=ReadExact[genes,Potential];
If[exact=={},Return[Null];,exact=exact//H;];
A=Init[population,genes,4];
For[j=1,j<=10000,j++,
For[i=1,i<=genes,i++,
A=Evolve[A,population,Distributions]
];
min=A[[1,1]];
If[A[[1,1]]<exact+10^(-tol),Break[]];
];
j
];


HaltingDatabase["CurieWeiss",stringEnsemble_,n_,tol_,All]:=
Module[{},
If[FindFile[
FileNameJoin[{workingdir,"HaltingDatabase","CurieWeiss",stringEnsemble,ToString[n]<>"-"<>ToString[tol]<>"-"<>ToString[\[Beta]]<>".m"}]
]===$Failed,
{},
Get[FileNameJoin[{workingdir,"HaltingDatabase","CurieWeiss",stringEnsemble,ToString[n]<>"-"<>ToString[tol]<>"-"<>ToString[\[Beta]]<>".m"}]]]
];
HaltingDatabase["CurieWeiss",stringEnsemble_,n_,tol_,m_]:=Module[{data,ii,add,counter},
data=HaltingDatabase["CurieWeiss",stringEnsemble,n,tol,All];

If[m>Length[data],
Print["Getting "<>ToString[m-Length[data]]<>" more samples."];

add=ParallelTable[
SampleHaltingTime["CurieWeiss",stringEnsemble,n,tol],{ii,m-Length[data]+10}
];

data=data~Join~add;
Put[data,FileNameJoin[{workingdir,"HaltingDatabase","CurieWeiss",stringEnsemble,ToString[n]<>"-"<>ToString[tol]<>"-"<>ToString[\[Beta]]<>".m"}]];
];
data[[;;m]]
];
SampleHaltingTime["CurieWeiss",string_,n_,r_]:=Module[{GetEnsemble,out,A},
If[string=="Original",GetEnsemble=c0];
If[string=="Cubic",GetEnsemble=c1];
If[string=="Shift",GetEnsemble=c2];
If[string=="Eight",GetEnsemble=c3];
If[string=="Remove",GetEnsemble=c4];
If[string=="test",GetEnsemble=test];
RunSimBin[n,r,GetEnsemble]
];


SampleHaltingTime["Toda",string_,n_,tol_]:=Module[{GetEnsemble,out,A},
If[string=="GOE",GetEnsemble=GOE];
If[string=="GUE",GetEnsemble=GUE];
If[string=="QUE",GetEnsemble=QUE];
If[string=="sGUE",GetEnsemble=sGUE];
If[string=="sQUE",GetEnsemble=sQUE];
If[string=="CoshUE", GetEnsemble=CoshUE];
If[string=="test",GetEnsemble=test];
If[string=="b12", GetEnsemble=b12];
If[string=="b13",GetEnsemble=b13];
If[string=="iid",GetEnsemble=iid];
If[string=="BE",GetEnsemble=BE];
A=GetEnsemble[n];
If[A==0,Print["# of requested samples exceeded # in SpectraDatabase, counter undefined or no SpectraDatabase entry exists"];
Return[Null];];
out=Toda[A,10^(-tol),.05]
];


SampleHaltingTime["LargestGap",string_,n_,tol_]:=Module[{GetEnsemble,out,A},
If[string=="GOE",GetEnsemble=GOE];
If[string=="GUE",GetEnsemble=GUE];
If[string=="QUE",GetEnsemble=QUE];
If[string=="sGUE",GetEnsemble=sGUE];
If[string=="sQUE",GetEnsemble=sQUE];
If[string=="CoshUE", GetEnsemble=CoshUE];
If[string=="test",GetEnsemble=test];
If[string=="b12", GetEnsemble=b12];
If[string=="b13",GetEnsemble=b13];
If[string=="iid",GetEnsemble=iid];
If[string=="BE",GetEnsemble=BE];
A=GetEnsemble[n];
If[A==0,Print["# of requested samples exceeded # in SpectraDatabase, counter undefined or no SpectraDatabase entry exists"];
Return[Null];];
LargestGap[A]
];
